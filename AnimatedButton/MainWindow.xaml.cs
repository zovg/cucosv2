﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using System.Windows.Threading;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Linq;
using CUCOS.Properties;

/*
 * Version
 * 0.84 - fix bug with color for text on manager button in idle
 *        picture for Logo set from catalog (res\nettoa.png)  
 * 0.86 - add:
 *          can to change position for function button (press btn SAVE)
 *          enable/disable function button (press btn SAVE)
 *          set SCOTill (press btn SAVE)
 *          enable/disable SCO mode (press btn SAVE)
 *          
 *          send data /till/0x/On for top button
 *          send data /till/0x/Off for bottom button
 *          can to change name for KASSE button 
 *          save all changes to iini file
 *          show version for soft - when you move cursor to text CUCOS, you can see hint text "v0.86"
 *          0.88
 *            changes:
 *          - in the lower case the "Open" command (in SCO mode). /till/04/set/open /till/04/set/close
 *          - the SCO button is hidden in the config menu
 *          - fixed the number of displayed buttons after exiting SCO mode 
 *          - fixed the code of the sent button (till)
 *          - locked button name changes in SCO mode
 * 0.89
        add select language via menu
        2-lang . Translate save in XML file [res/en.xml] and [res/de.xml].
        After select language need to reboot programm
   0.92
        add warning msg if no connection with server
        write log file for error connection with server
        
 */
namespace AnimatedButton
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>     
    class UserLang
    {  
        public string btn_Alarm { get; set; }
        public string btn_Snooze { get; set; }
        public string btn_MA { get; set; }
        public string btn_ML { get; set; }
        public string btn_OK { get; set; }
        public string btn_PAUSE { get; set; }        
    }

    public partial class MainWindow : Window
    {
        public bool connect_state;
        public int numberway;
        public bool SCOEnable;
        //StreamWriter felog = new StreamWriter("res/erlog.txt",true);
        string writePath = "res/erlog.txt";
        struct KasseCurState_
        {
            public string state;
            public int countBlink;
            public bool stateblink;
            public bool MANAGER;
            public bool BREAK;
            public bool CALLAC;
            public bool OK;

            public KasseCurState_(string state, int ncountBlink, bool nstateblink, bool nMANAGER, bool nBREAK, bool nCALLAC, bool nOK)
            {
                this.state = "unknown";
                this.countBlink = ncountBlink;
                this.stateblink = nstateblink;
                this.MANAGER = nMANAGER;
                this.BREAK = nBREAK;
                this.OK = nOK;
                this.CALLAC = nCALLAC;
            }
        }
        struct KasseCurStateFunction_
        {
            public int numberTill;

            public bool MANAGER;
            public bool BREAK;
            public bool CALLAC;
            public bool OK;
            public bool ALARM;
            public KasseCurStateFunction_(int nnumberTill, bool nMANAGER, bool nBREAK, bool nCALLAC, bool nOK, bool nALARM)
            {
                this.numberTill = nnumberTill;

                this.MANAGER = nMANAGER;
                this.BREAK = nBREAK;
                this.OK = nOK;
                this.CALLAC = nCALLAC;
                this.ALARM = nALARM;
            }
        }
        KasseCurState_[] KasseState = new KasseCurState_[12];
        KasseCurStateFunction_ KasseCurStateFunction = new KasseCurStateFunction_();
        //string[] KasseState = new string[12];
        private int counter500ms, count1sec, countServerErrorsec, countServerErrorsec_MAX;

        public UdpClient client = new UdpClient();
        UdpClient receive = new UdpClient();

        Button[] btns = new Button[18];
        private int listenPort = 8888;
        private int currentThill;
        private string IPAddrtxtbox;
        Thread ReceiverThread;
        UserLang btnXMLname = new UserLang();
        public class INIManager
        {
            //Конструктор, принимающий путь к INI-файлу
            public INIManager(string aPath)
            {
                path = aPath;
            }

            //Конструктор без аргументов (путь к INI-файлу нужно будет задать отдельно)
            public INIManager() : this("") { }

            //Возвращает значение из INI-файла (по указанным секции и ключу) 
            public string GetPrivateString(string aSection, string aKey)
            {
                //Для получения значения
                StringBuilder buffer = new StringBuilder(SIZE);

                //Получить значение в buffer
                GetPrivateString(aSection, aKey, null, buffer, SIZE, path);

                //Вернуть полученное значение
                return buffer.ToString();
            }

            //Пишет значение в INI-файл (по указанным секции и ключу) 
            public void WritePrivateString(string aSection, string aKey, string aValue)
            {
                //Записать значение в INI-файл
                WritePrivateString(aSection, aKey, aValue, path);
            }

            //Возвращает или устанавливает путь к INI файлу
            public string Path { get { return path; } set { path = value; } }

            //Поля класса
            private const int SIZE = 1024; //Максимальный размер (для чтения значения из файла)
            private string path = null; //Для хранения пути к INI-файлу

            //Импорт функции GetPrivateProfileString (для чтения значений) из библиотеки kernel32.dll
            [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileString")]
            private static extern int GetPrivateString(string section, string key, string def, StringBuilder buffer, int size, string path);

            //Импорт функции WritePrivateProfileString (для записи значений) из библиотеки kernel32.dll
            [DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileString")]
            private static extern int WritePrivateString(string section, string key, string str, string path);
        }

        public MainWindow()
        {
            
            InitializeComponent();
            Loaded += MainWindow_Load;
            double screenHeight = SystemParameters.FullPrimaryScreenHeight;
            double screenWidth = SystemParameters.FullPrimaryScreenWidth;
            List<UserLang> users = new List<UserLang>();

            Console.WriteLine("INNT FORM");
            //this.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
            //this.Top = 700 ;
            //this.Left = 300;

            DispatcherTimer timer1 = new DispatcherTimer();
            timer1.Tick += new EventHandler(timer1_Tick_1s);
            timer1.Interval = TimeSpan.FromMilliseconds(1000);
            timer1.Start();

 /*           DispatcherTimer timerStateKasse = new DispatcherTimer();
            timerStateKasse.Tick += new EventHandler(timerStateKasse_Tick_100ms);
            timerStateKasse.Interval = TimeSpan.FromMilliseconds(100);*/

            //------------ Timer 500msec ----------------------------
            counter500ms = 0;         
            count1sec = 0;
            countServerErrorsec = 0;
            countServerErrorsec_MAX = 10;
            for (int i = 0; i < 12; i++)
            {
                //KasseState[i] = "close";
                KasseState[i].state = "close";
                KasseState[i].BREAK = false;
                KasseState[i].MANAGER = false;
                KasseState[i].CALLAC = false;
                KasseState[i].OK = false;

            }
            KasseCurStateFunction.numberTill = int.Parse(txtBThTill.Text);
            KasseCurStateFunction.BREAK = false;
            KasseCurStateFunction.MANAGER = false;
            KasseCurStateFunction.CALLAC = false;
            KasseCurStateFunction.OK = false;


            btns[0] = BtnKasse1; btns[1] = BtnKasse2;
            btns[2] = BtnKasse3; btns[3] = BtnKasse4;
            btns[4] = BtnKasse5; btns[5] = BtnKasse6;
            btns[6] = BtnKasse7; btns[7] = BtnKasse8;

            btns[8] = btnAlarm; btns[9] = btnSnooze;
            btns[10] = btnMA; btns[11] = btnML;
            btns[12] = btnBreak; btns[13] = btnOk;
            btns[14] = BtnR1C6; btns[15] = BtnR1C7;
            btns[16] = BtnClose;
            btns[17] = BtnSave;           
           

            for (int i = 0; i < 8; i++)
                btns[i].Visibility = Visibility.Hidden;

            for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;


            
            
        }
        private void apply_language()
        {
                
 /*           XDocument doc = new XDocument(
         new XElement("library",
             new XElement("config",
                 new XElement("nm_config", "Config"),
                 new XElement("nm_ip_adress", "IP Adress"),
                 new XElement("nm_port_listen", "Port listen"),
                 new XElement("nm_port_send", "Port send"),
                 new XElement("nm_sco_tills", "SCO Tills"),
                 new XElement("nm_tills", "Tills"),
                 new XElement("nm_this_tills", "This Till"),
                 new XElement("nm_btn_save", "Save"),
                 new XElement("nm_way_language", "Language"),
                 new XElement("nm_way_short", "Short"),
                 new XElement("nm_way_long", "Long"),
                 new XElement("nm_position", "Position")),
             new XElement("botttom_btn",
                 new XElement("nm_alarm", "ALarm"),
                 new XElement("nm_snooze", "Snooze"),
                 new XElement("nm_pause", "Pause"),
                 new XElement("nm_ml", "ML"),
                 new XElement("nm_ma", "MA"),
                 new XElement("nm_ok", "OK")),
             new XElement("menu",
                 new XElement("nm_configuration", "Configuration"),
                 new XElement("nm_managerview", "Managerview"))));

            //сохраняем наш документ
            doc.Save(fileName);*/

            XmlDocument xDoc = new XmlDocument();
            if (cmbBoxLang.SelectedIndex == 1)
                xDoc.Load("res/en.xml");
            else if (cmbBoxLang.SelectedIndex == 2)
                xDoc.Load("res/de.xml");
            else if (cmbBoxLang.SelectedIndex == 0)
                xDoc.Load("res/en.xml");

            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode xnode in xRoot)
            {
                
                // получаем атрибут name
                if (xnode.Attributes.Count > 0)
                {
                    XmlNode attr = xnode.Attributes.GetNamedItem("config");
                    if (attr != null)
                    {
                        Console.WriteLine(attr.Value);
                    }
                }

                // обходим все дочерние узлы элемента user
                foreach (XmlNode childnode in xnode.ChildNodes)
                {

                    if (childnode.Name == "nm_port_listen")
                    {
                        Console.WriteLine($"nm_port_listen: {childnode.InnerText}");
                        lblPortlisten.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_ip_adress")
                    {
                        Console.WriteLine($"nm_ip_adress: {childnode.InnerText}");
                        lblIP.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_config")
                    {
                        Console.WriteLine($"nm_config: {childnode.InnerText}");
                        groupBoxConfig.Header = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_port_send")
                    {
                        Console.WriteLine($"nm_port_send: {childnode.InnerText}");
                        lblPortSend.Content = childnode.InnerText;
                    }                   
                    if (childnode.Name == "nm_tills")
                    {
                        Console.WriteLine($"nm_tills: {childnode.InnerText}");
                        lblTills.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_this_tills")
                    {
                        Console.WriteLine($"nm_this_tills: {childnode.InnerText}");
                        lbl_ThisTill.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_position")
                    {
                        Console.WriteLine($"nm_position: {childnode.InnerText}");
                        lblPosition.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_select_way")
                    {
                        Console.WriteLine($"nm_select_way: {childnode.InnerText}");
                        cmbBoxWay.Items[0] = childnode.InnerText;

                    }
                    if (childnode.Name == "nm_way_short")
                    {
                        Console.WriteLine($"nm_way_short: {childnode.InnerText}");
                        cmbBoxWay.Items[1]  = childnode.InnerText;

                    }
                    if (childnode.Name == "nm_way_long")
                    {
                        Console.WriteLine($"nm_way_long: {childnode.InnerText}");
                        cmbBoxWay.Items[2] = childnode.InnerText;

                    }
                    if (childnode.Name == "nm_way_language")
                    {
                        Console.WriteLine($"nm_way_language: {childnode.InnerText}");
                        cmbBoxLang.Items[0] = childnode.InnerText;

                    }
                    if (childnode.Name == "nm_sco_tills")
                    {
                        Console.WriteLine($"nm_sco_tills: {childnode.InnerText}");                        
                        //lblSCO.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_radius_button")
                    {
                        Console.WriteLine($"nm_radius_button: {childnode.InnerText}");
                        lblRadiusButton.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_font_size")
                    {
                        Console.WriteLine($"nm_font_size: {childnode.InnerText}");
                        lblFontSize.Content = childnode.InnerText;
                    }

                    if (xnode.Attributes.Count > 0)
                    {
                        XmlNode attr = xnode.Attributes.GetNamedItem("botttom_btn");
                        if (attr != null)
                        {
                            Console.WriteLine(attr.Value);
                            lblPortlisten.Content = attr.Value;
                        }
                    }
                    if (childnode.Name == "nm_alarm")
                    {
                        Console.WriteLine($"nm_alarm: {childnode.InnerText}");
                        //   btnAlarm.Content = childnode.InnerText;
                        btnXMLname.btn_Alarm = childnode.InnerText;
                        chkAlarm.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_snooze")
                    {
                        Console.WriteLine($"nm_snooze: {childnode.InnerText}");
                        //btnSnooze.Content = childnode.InnerText;
                        btnXMLname.btn_Snooze = childnode.InnerText;
                        chkSnooze.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_pause")
                    {
                        Console.WriteLine($"nm_pause: {childnode.InnerText}");
                        //btnBreak.Content = childnode.InnerText;
                        btnXMLname.btn_PAUSE = childnode.InnerText;
                        chkPause.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_ok")
                    {
                        Console.WriteLine($"nm_ok: {childnode.InnerText}");
                        //btnOk.Content = childnode.InnerText;
                        btnXMLname.btn_OK = childnode.InnerText;
                        chkOK.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_ma")
                    {
                        Console.WriteLine($"nm_ma: {childnode.InnerText}");
                        //btnMA.Content = childnode.InnerText;
                        btnXMLname.btn_MA = childnode.InnerText;
                        chkMA.Content = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_ml")
                    {
                        Console.WriteLine($"nm_ml: {childnode.InnerText}");
                        //btnML.Content = childnode.InnerText;
                        btnXMLname.btn_ML = childnode.InnerText;
                        chkML.Content = childnode.InnerText;
                    }
                    if (xnode.Attributes.Count > 0)
                    {
                        XmlNode attr = xnode.Attributes.GetNamedItem("menu");
                        if (attr != null)
                        {
                            Console.WriteLine(attr.Value);
                            //lblPortlisten.Content = attr.Value;
                        }
                    }
                    if (childnode.Name == "nm_configuration")
                    {
                        Console.WriteLine($"nm_configuration: {childnode.InnerText}");
                        menuConfig.Header = childnode.InnerText;

                    }
                    if (childnode.Name == "nm_managerview")
                    {
                        Console.WriteLine($"nm_managerview: {childnode.InnerText}");
                        menuManager.Header = childnode.InnerText;
                    }
                    if (childnode.Name == "nm_retail_systems")
                    {
                        Console.WriteLine($"nm_retail_systems: {childnode.InnerText}");
                        lblvirtual.Content = childnode.InnerText;
                    }
                    
                }
                Console.WriteLine();
            }
        }
        private void timer1_Tick_1sec(object sender, System.EventArgs e)
        {
            counter500ms = counter500ms + 1;
            /*string message1 = "500ms!" + counter500ms;
            *//*Test msg for time 500ms*//*
            if (connect_state)
            {
                byte[] data = Encoding.UTF8.GetBytes(message1);
                int numberOfSentBytes = receive.Send(data, data.Length);
            }*/
        }
        private void timerStateKasse_Tick_100ms(object sender, System.EventArgs e)
        {            
            
        }
        private void timer1_Tick_1s(object sender, System.EventArgs e)
        {
            if (SCOEnable)
            {
                for (int i = 0; i < int.Parse(txtSCoTill.Text); i++)
                {
                    btns[i].Background = Brushes.Green;
                    btns[i].Foreground = Brushes.Black;
                    //btns[i].Content = "ON";
                }
                for (int i = 8; i < int.Parse(txtSCoTill.Text)+8; i++)
                {
                    btns[i].Background = Brushes.Red;
                    btns[i].Foreground = Brushes.Black;
                    //btns[i].Content = "OFF";
                }
            }
            else
            {
                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                    switch (KasseState[i].state)
                    {
                        case "open":
                        case "OPEN":
                            if (((int.Parse(txtBThTill.Text) - 1) == i) && (KasseCurStateFunction.BREAK == true))
                            {

                            }
                            else
                                btns[i].Background = Brushes.Green;
                            break;
                        case "opening":
                        case "OPENING":
                            if (((int.Parse(txtBThTill.Text) - 1) == i) && (KasseCurStateFunction.BREAK == true))
                            {

                            }
                            else
                            {
                                if (btns[i].Background == Brushes.White)
                                {
                                    btns[i].Background = Brushes.Green;
                                    btns[i].Foreground = Brushes.White;
                                }
                                else
                                {
                                    btns[i].Background = Brushes.White;
                                    btns[i].Foreground = Brushes.Black;
                                }
                            }
                            break;

                        case "close":
                        case "CLOSE":
                            if (((int.Parse(txtBThTill.Text) - 1) == i) && (KasseCurStateFunction.BREAK == true))
                            {

                            }
                            else
                            {
                                btns[i].Background = Brushes.Red;
                                btns[i].Foreground = Brushes.White;//fix black Foreground
                            }
                            break;

                        case "closing":
                        case "CLOSING":
                            if (((int.Parse(txtBThTill.Text) - 1) == i) && (KasseCurStateFunction.BREAK == true))
                            {

                            }
                            else
                            {
                                if (btns[i].Background == Brushes.White)
                                {
                                    btns[i].Background = Brushes.Red;
                                    btns[i].Foreground = Brushes.White;
                                }
                                else
                                {
                                    btns[i].Background = Brushes.White;
                                    btns[i].Foreground = Brushes.Black;
                                }
                            }
                            break;
                        case "MANAGER":
                            break;
                        case "CALLAC":
                            break;
                        case "BREAK":
                            break;
                        case "OK":
                            break;
                        default:
                            break;
                    }
                }


                if (KasseCurStateFunction.CALLAC)
                {
                    if (btnMA.Background == Brushes.Gray)
                    {
                        btnMA.Background = Brushes.White;
                        btnMA.Foreground = Brushes.Black;
                    }
                    else
                    {
                        btnMA.Background = Brushes.Gray;
                        btnMA.Foreground = Brushes.White;
                    }
                }
                else
                {
                    btnMA.Background = Brushes.Gray;
                    btnMA.Foreground = Brushes.White;
                }


                if (KasseCurStateFunction.MANAGER)
                {
                    if (btnML.Background == Brushes.Gray)
                    {
                        btnML.Background = Brushes.White;
                        btnML.Foreground = Brushes.Black;
                    }
                    else
                    {
                        btnML.Background = Brushes.Gray;
                        btnML.Foreground = Brushes.White;
                    }
                }
                else
                {
                    btnML.Background = Brushes.Gray;
                    btnML.Foreground = Brushes.White;
                }

                if (KasseCurStateFunction.ALARM)
                {
                    if (btnAlarm.Background == Brushes.Gray)
                    {
                        btnAlarm.Background = Brushes.Red;
                    }
                    else
                        btnAlarm.Background = Brushes.Gray;
                }
                else
                {
                    btnAlarm.Background = Brushes.Gray;
                    btnAlarm.Foreground = Brushes.White;
                }

                if (KasseCurStateFunction.BREAK)
                {
                    if ((btns[int.Parse(txtBThTill.Text) - 1].Background == Brushes.Green) || (btns[int.Parse(txtBThTill.Text) - 1].Background == Brushes.Red))
                    {
                        btns[int.Parse(txtBThTill.Text) - 1].Background = Brushes.White;
                        btns[int.Parse(txtBThTill.Text) - 1].Foreground = Brushes.Black;
                    }
                    else
                    {
                        btns[int.Parse(txtBThTill.Text) - 1].Background = Brushes.Red;
                        btns[int.Parse(txtBThTill.Text) - 1].Foreground = Brushes.White;
                    }

                    btnBreak.Background = Brushes.Red;
                }
                else
                {
                    btnBreak.Background = Brushes.Gray;
                    btnBreak.Foreground = Brushes.White;
                }
            }
            count1sec++;
            countServerErrorsec++;

            if (count1sec == 5)
            {
                /* get update state */
                Send_Get_State();
                count1sec = 0;
            }
            if( countServerErrorsec == countServerErrorsec_MAX)
            {
                string timeError = DateTime.Now.ToString();
                using (StreamWriter felog = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {                           
                    countServerErrorsec_MAX = 3600;
                    if (cmbBoxLang.SelectedIndex == 2)
                    {
                        felog.Write($"{timeError} - Keine Verbindung zum Serverr" + "\r\n");
                        
                        MessageBox.Show("Keine Verbindung zum Serverr", "Serverfehler", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        felog.Write($"{timeError} - No сonnection to server" + "\r\n");
                        MessageBox.Show("No сonnection to server", "Server error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }   
                               
                            
            }
        }

        public void Receiver()
        {
            bool done = true;            
            int ainState = 0;
            int numberThill_get;

            while (true)
            {
                UdpClient listener = new UdpClient(listenPort);
                IPEndPoint groupEP = new IPEndPoint(IPAddress.Parse(IPAddrtxtbox)/*IPAddress.Any*/, listenPort);
                string received_data;
                byte[] receive_byte_array;
                try
                {
                    while (done)
                    {
                        string[] status_kasse = { "open", "opening", "close", "closing", "CALLAC", "MANAGER", "BREAK", "OK", "ALARM", "manager", "break", "ok", "alarm", "callac", };
                        string[] delimiterChars = { "/","$" };
                        string status;
                        /* temp state Function button for next update*/
                        bool rec_MANAGER = false;
                        bool rec_BREAK = false;
                        bool rec_OK = false;
                        bool rec_CALLAC = false;
                        bool rec_ALARM = false;

                        receive_byte_array = listener.Receive(ref groupEP);
                        received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
                        string[] words = received_data.Split(delimiterChars, System.StringSplitOptions.RemoveEmptyEntries);
                        System.Console.WriteLine($" Receive state-<{received_data}>");
                        
                        foreach (var word in words)
                        {

                            string tmp_data;
                            tmp_data = word;
                           
                            switch (tmp_data)
                            {
                                case "till":
                                            countServerErrorsec = 0;//reset counter for error msg for user
                                            countServerErrorsec_MAX = 10;
                                    break;

                                default:
                                    if (word.Length != 0)
                                    {

                                        if (int.TryParse(word.Substring(0, 2), out ainState))
                                        {
                                            int.TryParse(word.Substring(0, 2), out numberThill_get);
                                            status = word.Substring(2);
                                            //System.Console.WriteLine($"number-<{ainState}> status-<{status}>");
                                            switch (status)
                                            {
                                                case "open":
                                                case "OPEN":
                                                    //btns[ainState - 1].Background = Brushes.Green;// FromArgb(0x45, 0xA1, 0x2A);
                                                    KasseState[ainState - 1].state = status;
                                                    KasseState[ainState - 1].stateblink = true;
                                                    break;
                                                case "opening":
                                                case "OPENING":
                                                    //btns[ainState - 1].Background = Brushes.Green;//FromArgb(0x45, 0xA1, 0x2A);
                                                    KasseState[ainState - 1].state = status;
                                                    KasseState[ainState - 1].stateblink = true;
                                                    break;
                                                case "close":
                                                case "CLOSE":
                                                    //btns[ainState - 1].Background = Brushes.Red;//FromArgb(255, 0, 0);
                                                    KasseState[ainState - 1].state = status;
                                                    KasseState[ainState - 1].stateblink = true;
                                                    break;
                                                case "closing":
                                                case "CLOSING":
                                                    //btns[ainState - 1].Background = Brushes.Red;// FromArgb(255, 0, 0);
                                                    KasseState[ainState - 1].state = status;
                                                    KasseState[ainState - 1].stateblink = true;
                                                    break;
                                                case "CALLAC":
                                                case "callac":
                                                    /*When you get xxCALLAC in second row the lila button starts flashing.Look to screeny.
                                                    * When somebody push this Button you send / till / xx / set / CALLAC*/
                                                    if (currentThill == ainState)
                                                    {
                                                        KasseState[ainState - 1].CALLAC = true;
                                                    }
                                                    //KasseState[ainState - 1].state = status;                                                    
                                                    rec_CALLAC = true;
                                                    break;
                                                case "MANAGER":
                                                case "manager":
                                                    /*When you get in answear xxMANAGER orange Button in second starts flashing. 
                                                     * When somebody push this Button you send /till/xx/set/MANAGER */
                                                    if (currentThill == ainState)
                                                    {
                                                        KasseState[ainState - 1].MANAGER = true;
                                                        rec_MANAGER = true;
                                                    }
                                                    break;
                                                case "BREAK":
                                                case "break":
                                                    if (currentThill == ainState)
                                                    {
                                                        /*When you get xxbreak you have to make the button in first row Brown. 
                                                         * xx is till (button) number. When somebody push this button you have to send /till/xx/set/break.*/
                                                        //KasseState[ainState - 1] = status;
                                                        KasseState[ainState - 1].BREAK = true;
                                                        //btns[ainState - 1].Background = Brushes.Red;//FromArgb(0x96, 0x4B, 0x00);
                                                        KasseCurStateFunction.BREAK = true;
                                                        rec_BREAK = true;
                                                    }
                                                    break;
                                                case "OK":
                                                case "ok":
                                                    if (currentThill == numberThill_get)
                                                    {
                                                        KasseState[ainState - 1].OK = true;
                                                        /*OK Button: When somebody push ok and this till is closed you send / till / xx / set / open
                                                        If this till status is open you send /till/xx/set/closed
                                                        */
                                                        KasseCurStateFunction.OK = true;
                                                    }
                                                    rec_OK = true;
                                                    break;
                                                case "QALARM":
                                                case "qalarm":
                                                    if (currentThill == numberThill_get)
                                                    {
                                                        rec_ALARM = true;
                                                    }
                                                    break;
                                            }
                                         /*   KasseCurStateFunction.MANAGER = rec_MANAGER;
                                            KasseCurStateFunction.OK = rec_OK;
                                            KasseCurStateFunction.BREAK = rec_BREAK;
                                            KasseCurStateFunction.CALLAC = rec_CALLAC;
                                            KasseCurStateFunction.ALARM = rec_ALARM;*/
                                        }
                                        KasseCurStateFunction.MANAGER = rec_MANAGER;
                                        KasseCurStateFunction.OK = rec_OK;
                                        KasseCurStateFunction.BREAK = rec_BREAK;
                                        KasseCurStateFunction.CALLAC = rec_CALLAC;
                                        KasseCurStateFunction.ALARM = rec_ALARM;
                                    }
                                    break;
                            }
                            if (word.Length != 0)
                            {

                               // System.Console.WriteLine($"<{word}>");

                            }
                        }
                        //Send_Get_State();
                        //Console.WriteLine(received_data);
                    }

                }
                catch (Exception ee)
                {
                    //Console.WriteLine(ee.ToString());
                }
                listener.Close();

                Thread.Sleep(1000);
            }
        }
        private void resizeWindow()
        {
            if (int.Parse(txtRadiusButton.Text) < 43)
                txtRadiusButton.Text = "43";
            if (int.Parse(txtRadiusButton.Text) > 100)
                txtRadiusButton.Text = "100";
            for (int i = 0; i < 16; i++)
            {
                btns[i].FontSize = int.Parse(txtFontSizeButton.Text);
                btns[i].Width = int.Parse(txtRadiusButton.Text) * 2;
                btns[i].Height = int.Parse(txtRadiusButton.Text) * 2;
            }

            StackPanelGlobal.Width = (btns[0].Width + 10) * 8;
            
                StackPanelGlobal.Width = StackPanelGlobal.Width + 35;
            StackBottomSCO.Width = StackPanelGlobal.Width - btns[0].Width / 2;
            WrapPanelGlobal.Width = StackPanelGlobal.Width;
            WindowCUCOS.Width = StackPanelGlobal.Width;
            GridConfig.Width = StackPanelGlobal.Width;

            StackPanelGlobal.Height = 386 + (btns[0].Height - 85) * 2;
            WrapPanelGlobal.Height = StackPanelGlobal.Height;
            WindowCUCOS.Height = StackPanelGlobal.Height;
            //GridConfig.Height = StackPanelGlobal.Height;
            double width_name_kasse_txt;
            width_name_kasse_txt = btns[0].Width + 10;
            txtNameButton1.Margin = new Thickness(width_name_kasse_txt / 3, btns[0].Height / 2, 0, 0);
            txtNameButton2.Margin = new Thickness(width_name_kasse_txt + width_name_kasse_txt / 3, btns[0].Height / 2, 0, 0);
            txtNameButton3.Margin = new Thickness(width_name_kasse_txt * 2 + width_name_kasse_txt / 3, btns[0].Height / 2, 0, 0);
            txtNameButton4.Margin = new Thickness(width_name_kasse_txt * 3 + width_name_kasse_txt / 3, btns[0].Height / 2, 0, 0);
            txtNameButton5.Margin = new Thickness(width_name_kasse_txt * 4 + width_name_kasse_txt / 3, btns[0].Height / 2, 0, 0);
            txtNameButton6.Margin = new Thickness(width_name_kasse_txt * 5 + width_name_kasse_txt / 3, btns[0].Height / 2, 0, 0);
            txtNameButton7.Margin = new Thickness(width_name_kasse_txt * 6 + width_name_kasse_txt / 3, btns[0].Height / 2, 0, 0);
            txtNameButton8.Margin = new Thickness(width_name_kasse_txt * 7 + width_name_kasse_txt / 3, btns[0].Height / 2, 0, 0);
/*
            txtNameButton1.Height = btns[0].Height/3;
            txtNameButton1.Width = btns[0].Width / 1.2;
            txtNameButton1.FontSize = int.Parse(txtFontSizeButton.Text);

            txtNameButton3.Height = btns[0].Height / 3;
            txtNameButton3.Width = btns[0].Width / 1.2;
            txtNameButton6.Height = btns[0].Height / 3;
            txtNameButton6.Width = btns[0].Width / 1.2;*/
        }
        private void createConnect()
        {
            client.Connect(txtBIP.Text, int.Parse(txtBPort.Text));
            connect_state = true;
        }
        private void MainWindow_Load(object sender, RoutedEventArgs e)
        {
            client.Connect(txtBIP.Text, int.Parse(txtBPort.Text));
            groupBoxConfig.Visibility = Visibility.Hidden;
            DockPanelManager.Visibility = Visibility.Visible;

            /*read NET config*/
            string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";           
            string s = "text"; // эта переменная обязательно должна быть не пустой.
                               // нельзя задать её просто string s; Почему? понятия не имею, но если переменная изначально пустая,
                               // функция фозвращает длину ключа а не его значение.
            INIManager inimanager = new INIManager(path);
            //Console.WriteLine("INI file path  = " + path);
            //Получить значение по ключу name из секции main
            txtBIP.Text = inimanager.GetPrivateString("net", "IP");
            txtBPort.Text = inimanager.GetPrivateString("net", "Port");
            txtBTill.Text = inimanager.GetPrivateString("net", "Tills");
            txtBThTill.Text = inimanager.GetPrivateString("net", "NTill");
            txtBPort_Listen.Text = inimanager.GetPrivateString("net", "PortList");


            if (inimanager.GetPrivateString("net", "buttonFontSize") == "")
            { txtFontSizeButton.Text = "8"; inimanager.WritePrivateString("net", "buttonFontSize", "16"); }
            else txtFontSizeButton.Text = inimanager.GetPrivateString("net", "buttonFontSize");

            if (inimanager.GetPrivateString("net", "buttonRadius") == "")
            { txtRadiusButton.Text = "43"; inimanager.WritePrivateString("net", "buttonRadius", "43"); }
            else txtRadiusButton.Text = inimanager.GetPrivateString("net", "buttonRadius");

            if (inimanager.GetPrivateString("net", "SCOTill") == "")
            { txtSCoTill.Text = "8"; inimanager.WritePrivateString("net", "SCOTill","8"); }
            else txtSCoTill.Text = inimanager.GetPrivateString("net", "SCOTill");

            if (inimanager.GetPrivateString("net", "Position1") == "")
            { txtNmBtnPos1.Text = "1"; inimanager.WritePrivateString("net", "Position1","1"); }
            else txtNmBtnPos1.Text = inimanager.GetPrivateString("net", "Position1");

            if (inimanager.GetPrivateString("net", "Position2") == "")
            { txtNmBtnPos2.Text = "2"; inimanager.WritePrivateString("net", "Position2", "2"); }
            else txtNmBtnPos2.Text = inimanager.GetPrivateString("net", "Position2");

            if(inimanager.GetPrivateString("net", "Position3") == "")
            { txtNmBtnPos3.Text = "3";  inimanager.WritePrivateString("net", "Position3","3"); }         else txtNmBtnPos3.Text = inimanager.GetPrivateString("net", "Position3");
           
            if(inimanager.GetPrivateString("net", "Position4") == "")
            {   txtNmBtnPos4.Text = "4";  inimanager.WritePrivateString("net", "Position4","4"); }         else txtNmBtnPos4.Text = inimanager.GetPrivateString("net", "Position4");
            if(inimanager.GetPrivateString("net", "Position5")== "")
            {   txtNmBtnPos5.Text = "5";  inimanager.WritePrivateString("net", "Position5","5"); }         else txtNmBtnPos5.Text = inimanager.GetPrivateString("net", "Position5");
            if(inimanager.GetPrivateString("net", "Position6")== "")
            {   txtNmBtnPos6.Text = "6";  inimanager.WritePrivateString("net", "Position6","6"); }         else txtNmBtnPos6.Text = inimanager.GetPrivateString("net", "Position6");

            if(inimanager.GetPrivateString("net", "chkAlarm") == "True")
            chkAlarm.IsChecked = true; else chkAlarm.IsChecked = false;
            if (inimanager.GetPrivateString("net", "chkMA") == "True")
                chkMA.IsChecked = true;

            if (inimanager.GetPrivateString("net", "chkML") == "True") 
                chkML.IsChecked = true;
            if (inimanager.GetPrivateString("net", "chkOK") == "True") 
                chkOK.IsChecked = true;
            if (inimanager.GetPrivateString("net", "chkPause") == "True") 
                chkPause.IsChecked = true;
            if (inimanager.GetPrivateString("net", "chkSnooze") == "True") 
                chkSnooze.IsChecked = true;


            if (inimanager.GetPrivateString("net", "nameKASSE1") == "")
            {
                BtnKasse1.Content = "KASSE1";
                inimanager.WritePrivateString("net", "nameKASSE5", "KASSE1");
            }
            else
                BtnKasse1.Content = inimanager.GetPrivateString("net", "nameKASSE1");
            /*  2   */
            if (inimanager.GetPrivateString("net", "nameKASSE2") == "")
            {
                BtnKasse2.Content = "KASSE2";
                inimanager.WritePrivateString("net", "nameKASSE5", "KASSE2");
            }
            else
                BtnKasse2.Content = inimanager.GetPrivateString("net", "nameKASSE2");
            /*--------------------------------------3   */
            if (inimanager.GetPrivateString("net", "nameKASSE3") == "")
            {
                BtnKasse3.Content = "KASSE3";
                inimanager.WritePrivateString("net", "nameKASSE5", "KASSE3");
            }
            else
                BtnKasse3.Content = inimanager.GetPrivateString("net", "nameKASSE3");
            /*-------------------------------------- 4   */
            if (inimanager.GetPrivateString("net", "nameKASSE4") == "")
            {
                BtnKasse4.Content = "KASSE4";
                inimanager.WritePrivateString("net", "nameKASSE5", "KASSE4");
            }
            else
                BtnKasse4.Content = inimanager.GetPrivateString("net", "nameKASSE4");
            /*-------------------------------------- 5   */
            if (inimanager.GetPrivateString("net", "nameKASSE5") == "")
            {
                BtnKasse5.Content = "KASSE5";
                inimanager.WritePrivateString("net", "nameKASSE5","KASSE5");
            }
            else
                BtnKasse5.Content = inimanager.GetPrivateString("net", "nameKASSE5");
            /*-------------------------------------- 6   */
            if (inimanager.GetPrivateString("net", "nameKASSE6") == "")
            {
                BtnKasse6.Content = "KASSE6";
                inimanager.WritePrivateString("net", "nameKASSE5", "KASSE6");
            }
            else
                BtnKasse6.Content = inimanager.GetPrivateString("net", "nameKASSE6");
            /*-------------------------------------- 7   */
            if (inimanager.GetPrivateString("net", "nameKASSE7") == "")
            {
                BtnKasse7.Content = "KASSE7";
                inimanager.WritePrivateString("net", "nameKASSE5", "KASSE7");
            }
            else
                BtnKasse7.Content = inimanager.GetPrivateString("net", "nameKASSE7");
            /*-------------------------------------- 8   */
            if (inimanager.GetPrivateString("net", "nameKASSE8") == "")
            {
                BtnKasse8.Content = "KASSE8";
                inimanager.WritePrivateString("net", "nameKASSE5", "KASSE8");
            }
            else
                BtnKasse8.Content = inimanager.GetPrivateString("net", "nameKASSE8");

            IPAddrtxtbox = txtBIP.Text;
            s = inimanager.GetPrivateString("net", "way");
            if(s == "Short")
                cmbBoxWay.SelectedIndex = 1;
            else if (s == "Long")
                cmbBoxWay.SelectedIndex = 2;

            s = inimanager.GetPrivateString("net", "language");
            if (s == "English")
                cmbBoxLang.SelectedIndex = 1;
            else if (s == "Deutsche")
                cmbBoxLang.SelectedIndex = 2;

            if (txtBIP.Text == "")
            {
                inimanager.WritePrivateString("net", "IP", "192.168.0.100");
                txtBIP.Text = "192.168.0.100";
            }
            if (txtBPort.Text == "")
            {
                inimanager.WritePrivateString("net", "Port", "8888");
                txtBPort.Text = "8888";
            }
            if (txtBPort_Listen.Text == "")
            {
                inimanager.WritePrivateString("net", "PortList", "8887");
                txtBPort.Text = "8887";
            }
            if (txtBTill.Text == "")
            {
                inimanager.WritePrivateString("net", "Tills", "8");
                txtBTill.Text = "8";
            }
            if (txtBThTill.Text == "")
            {
                inimanager.WritePrivateString("net", "NTill", "3");
                txtBThTill.Text = "3";
            }

            apply_language();
            s = inimanager.GetPrivateString("net", "way");
            if (s == "Short")
                cmbBoxWay.SelectedIndex = 1;
            else if (s == "Long")
                cmbBoxWay.SelectedIndex = 2;

            btnML.Content = btnXMLname.btn_ML;
            btnMA.Content = btnXMLname.btn_MA;
            btnOk.Content = btnXMLname.btn_OK;
            btnAlarm.Content = btnXMLname.btn_Alarm;
            btnSnooze.Content = btnXMLname.btn_Snooze;
            btnBreak.Content = btnXMLname.btn_PAUSE;

            for (int i = 0; i < 8; i++)
            {
                btns[i].Visibility = Visibility.Hidden;
            }

            for (int i = 0; i < Convert.ToInt32(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;

            for (int i = 8; i < 16; i++)
            {
                btns[i].Visibility = Visibility.Hidden;
                
             
            }

            resizeWindow();

            if ((Convert.ToInt32(txtNmBtnPos1.Text) >= 1) && (Convert.ToInt32(txtNmBtnPos1.Text) <= 8))
                Grid.SetColumn(btnAlarm, Convert.ToInt32(txtNmBtnPos1.Text)-1);
            else
                chkAlarm.IsChecked = false;

            if ((Convert.ToInt32(txtNmBtnPos2.Text) >= 1) && (Convert.ToInt32(txtNmBtnPos2.Text) <= 8))
                Grid.SetColumn(btnSnooze, Convert.ToInt32(txtNmBtnPos2.Text)-1);
            else
                chkSnooze.IsChecked = false;

            if ((Convert.ToInt32(txtNmBtnPos4.Text) >= 1) && (Convert.ToInt32(txtNmBtnPos4.Text) <= 8))
                Grid.SetColumn(btnML, Convert.ToInt32(txtNmBtnPos4.Text)-1);
            else
                chkML.IsChecked = false;

            if ((Convert.ToInt32(txtNmBtnPos3.Text) >= 1) && (Convert.ToInt32(txtNmBtnPos3.Text) <= 8))
                Grid.SetColumn(btnMA, Convert.ToInt32(txtNmBtnPos3.Text)-1);
            else
                chkMA.IsChecked = false;

            if ((Convert.ToInt32(txtNmBtnPos5.Text) >= 1) && (Convert.ToInt32(txtNmBtnPos5.Text) <= 8))
                Grid.SetColumn(btnBreak, Convert.ToInt32(txtNmBtnPos5.Text)-1);
            else
                chkPause.IsChecked = false;

            if ((Convert.ToInt32(txtNmBtnPos6.Text) >= 1) && (Convert.ToInt32(txtNmBtnPos6.Text) <= 8))
                Grid.SetColumn(btnOk, Convert.ToInt32(txtNmBtnPos6.Text)-1);
            else
                chkOK.IsChecked = false;

            if (chkML.IsChecked == true)
                btnML.Visibility = Visibility.Visible;           

            if (chkOK.IsChecked == true)
                btnOk.Visibility = Visibility.Visible;            

            if (chkMA.IsChecked == true)
                btnMA.Visibility = Visibility.Visible;            

            if (chkPause.IsChecked == true)
                btnBreak.Visibility = Visibility.Visible;            

            if (chkAlarm.IsChecked == true)
                btnAlarm.Visibility = Visibility.Visible;            

            if (chkSnooze.IsChecked == true)
                btnSnooze.Visibility = Visibility.Visible;

            currentThill = int.Parse(txtBThTill.Text);
            ReceiverThread = new Thread(new ThreadStart(Receiver));
            ReceiverThread.IsBackground = true;
            ReceiverThread.Start();
            listenPort = Convert.ToInt32(txtBPort_Listen.Text);

            /*create UDP connect*/

            string message = "/till/#";
            byte[] data = Encoding.UTF8.GetBytes(message);
            try
            {
                client.Connect(txtBIP.Text, int.Parse(txtBPort.Text));
                connect_state = true;

            }
            catch (SocketException ersend)
            {
                if (ersend.ErrorCode == 10061)
                    Console.WriteLine("Port is closed");
                else if (ersend.ErrorCode == 10060)
                    Console.WriteLine("TimeOut");
                else Console.WriteLine(ersend.Message);
            }
            int numberOfSentBytes = client.Send(data, data.Length);
        }
        private void ConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int pos_btn;
            for (int i = 0; i < 8; i++)
                btns[i].Visibility = Visibility.Hidden;

            for (int i = 0; i < Convert.ToInt32(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;                  

            numberway = cmbBoxWay.SelectedIndex;
            if (numberway == 1)/*short way no. Not hidden. Only the letters ok should be away*/
                btnOk.Content = "";
            else
                btnOk.Content = "OK";
            /* изменение позиции кнопки в зависимости от пользовательских настроек*/
            pos_btn = Convert.ToInt32(txtNmBtnPos1.Text) - 1;
            Grid.SetColumn(btnAlarm, pos_btn);
            pos_btn = Convert.ToInt32(txtNmBtnPos2.Text) - 1;
            Grid.SetColumn(btnSnooze, pos_btn);
            pos_btn = Convert.ToInt32(txtNmBtnPos4.Text) - 1;
            Grid.SetColumn(btnML, pos_btn);
            pos_btn = Convert.ToInt32(txtNmBtnPos3.Text) - 1;
            Grid.SetColumn(btnMA, pos_btn);
            pos_btn = Convert.ToInt32(txtNmBtnPos5.Text) - 1;
            Grid.SetColumn(btnBreak, pos_btn);
            pos_btn = Convert.ToInt32(txtNmBtnPos6.Text) - 1;
            Grid.SetColumn(btnOk, pos_btn);

            if (chkML.IsChecked == true)
                btnML.Visibility = Visibility.Visible;
            else
                btnML.Visibility = Visibility.Hidden;

            if (chkOK.IsChecked == true)
                btnOk.Visibility = Visibility.Visible;
            else
                btnOk.Visibility = Visibility.Hidden;

            if (chkMA.IsChecked == true)
                btnMA.Visibility = Visibility.Visible;
            else
                btnMA.Visibility = Visibility.Hidden;

            if (chkPause.IsChecked == true)
                btnBreak.Visibility = Visibility.Visible;
            else
                btnBreak.Visibility = Visibility.Hidden;

            if (chkAlarm.IsChecked == true)
                btnAlarm.Visibility = Visibility.Visible;
            else
                btnAlarm.Visibility = Visibility.Hidden;

            if (chkSnooze.IsChecked == true)
                btnSnooze.Visibility = Visibility.Visible;
            else
                btnSnooze.Visibility = Visibility.Hidden;

            groupBoxConfig.Visibility = Visibility.Hidden;
            DockPanelManager.Visibility = Visibility.Visible;

            currentThill = int.Parse(txtBThTill.Text);
            Check_button_to_visible();
            btnSCO.Visibility = Visibility.Visible;

            resizeWindow();
        }
        private void ManagerviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBoxConfig.Visibility = Visibility.Visible;
            DockPanelManager.Visibility = Visibility.Hidden;
            btnSCO.Visibility = Visibility.Hidden;

            currentThill = int.Parse(txtBThTill.Text);
            Check_button_to_visible();
        }
        public string Send_OK(int numberKasse)
        {
            string answer = "unknown";
            string number = "0" + numberKasse;
            numberKasse -= 1;
            numberway = cmbBoxWay.SelectedIndex;
            if (connect_state)
            {
                /* KasseState.KsAr[numberKasse] - its current state  
                 *  numberway :
                 * 1 = short
                 * 2 = long
                 * **/
                switch (KasseState[numberKasse].state)
                {
                    case "open":
                    case "OPEN":
                        if (numberway == 1)
                        {
                        }
                        else
                        {
                            answer = "/till/" + number + "/set/close";
                            KasseState[numberKasse].state = "close";
                        }
                        break;
                    case "opening":
                    case "OPENING":
                        if (numberway == 1)
                        {
                            
                        }
                        else
                        {
                            answer = "/till/" + number + "/set/open";
                            KasseState[numberKasse].state = "open";
                        }
                        break;
                    case "close":
                    case "CLOSE":
                        if (numberway == 1)
                        {
                         
                        }
                        else
                        {
                            answer = "/till/" + number + "/set/open";
                            KasseState[numberKasse].state = "open";
                        }
                        break;
                    case "closing":
                    case "CLOSING":
                        if (numberway == 1)
                        {
                            
                        }
                        else
                        {
                            answer = "/till/" + number + "/set/close";
                            KasseState[numberKasse].state = "close";
                        }
                        break;      
                    default:
                        break;
                }

                System.Console.WriteLine($" Answer state-<{answer}>");
                byte[] data = Encoding.UTF8.GetBytes(answer);
                int numberOfSentBytes = client.Send(data, data.Length);

                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                }
            }
            else
                MessageBox.Show("Could not create connection.", "Error Connection", MessageBoxButton.OK, MessageBoxImage.Warning);
            return answer;
        }
        public void Send_SCO_CTRL(int numberKasse, string state)
        {
            string answer = "unknown";
            string number = "0" + numberKasse;
           

            if (numberKasse > 9)
            {
                number = "";
                number = Convert.ToString(numberKasse);
            }
            answer = "/till/" + number + "/set/" + state;
            System.Console.WriteLine($" SCO Answer state-<{answer}>");
            byte[] data = Encoding.UTF8.GetBytes(answer);
            int numberOfSentBytes = client.Send(data, data.Length);
        }
            public string Send_state_Kasse(int numberKasse)
        {
            string answer = "unknown";
            int numberSCOKasse;
            string number;
            numberSCOKasse = int.Parse(txtBTill.Text) + numberKasse;            
            number = "0" + numberKasse;
            numberKasse -= 1;
            // numberway = cmbBoxWay.SelectedIndex;

            if ((connect_state))
            {
                /* KasseState.KsAr[numberKasse] - its current state  
                 *  numberway :
                 * 1 = short
                 * 2 = long
                 * **/
                if (SCOEnable)
                {
                    
                    //Send_SCO_CTRL(numberKasse, "On");
                    
                    number = "";
                    if (numberSCOKasse > 9)
                    {                        
                        number = Convert.ToString(numberSCOKasse);
                    }
                    else
                    {                        
                        number = "0" + numberSCOKasse;
                    }
                    answer = "/till/" + number + "/set/open";

                    System.Console.WriteLine($" SCO Answer state-<{answer}>");
                    byte[] data = Encoding.UTF8.GetBytes(answer);
                    int numberOfSentBytes = client.Send(data, data.Length);
                }
                else
                {
                    if (((int.Parse(txtBThTill.Text) - 1) != numberKasse) || (KasseCurStateFunction.BREAK != true))
                    {
                        switch (KasseState[numberKasse].state)
                        {
                            case "open":
                            case "OPEN":
                                if (numberway == 1)
                                {
                                    answer = "/till/" + number + "/set/close";
                                    KasseState[numberKasse].state = "close";
                                }
                                else
                                {
                                    answer = "/till/" + number + "/set/closing";
                                    KasseState[numberKasse].state = "closing";
                                }
                                break;
                            case "opening":
                            case "OPENING":
                                if (numberway == 1)//short
                                {
                                    answer = "/till/" + number + "/set/open";
                                    KasseState[numberKasse].state = "open";
                                }
                                else
                                {
                                    answer = "/till/" + number + "/set/close";
                                    KasseState[numberKasse].state = "close";
                                }
                                break;
                            case "close":
                            case "CLOSE":
                                if (numberway == 1)
                                {
                                    answer = "/till/" + number + "/set/open";
                                    KasseState[numberKasse].state = "open";
                                }
                                else
                                {
                                    answer = "/till/" + number + "/set/opening";
                                    KasseState[numberKasse].state = "opening";
                                }
                                break;
                            case "closing":
                            case "CLOSING":
                                if (numberway == 1)
                                {
                                    answer = "/till/" + number + "/set/close";
                                    KasseState[numberKasse].state = "close";
                                }
                                else
                                {
                                    answer = "/till/" + number + "/set/opening";
                                    KasseState[numberKasse].state = "close";
                                }
                                break;
                            case "BREAK":
                            case "break":
                                answer = "/till/" + number + "/set/BREAK";
                                KasseState[numberKasse].state = "BREAK";
                                break;
                            case "MANAGER":
                            case "manager":
                                answer = "/till/" + number + "/set/MANAGER";
                                break;
                            case "CALLAC":
                            case "callac":
                                answer = "/till/" + number + "/set/CALLAC";
                                break;
                            default:
                                break;
                        }
                    }
                    System.Console.WriteLine($" Answer state-<{answer}>");
                    byte[] data = Encoding.UTF8.GetBytes(answer);
                    int numberOfSentBytes = client.Send(data, data.Length);

                    for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                    {

                    }
                }
            }
            else
            {
                if (!connect_state)
                    MessageBox.Show("Could not create connection.", "Error Connection", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return answer;
        }
        private void BtnKasse1_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(1);
        }
        private void BtnKasse2_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(2);
        }
        private void BtnKasse3_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(3);
        }
        private void BtnKasse4_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(4);
        }
        private void BtnKasse5_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(5);
        }
        private void BtnKasse6_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(6);
        }
        private void BtnKasse7_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(7);
        }
        private void BtnKasse8_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(8);            
        }

        private void defaultInstance_WindowClosed(object sender, EventArgs e)
        {
            client.Close();
        }
        public string Send_Get_State()
        {
            string answer = "unknown";

            answer = "/till/#"; //fix 1 bug

            if (connect_state)
            {
                System.Console.WriteLine($" Answer state-<{answer}>");
                byte[] data = Encoding.UTF8.GetBytes(answer);
                int numberOfSentBytes = client.Send(data, data.Length);

                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                }
            }

            return answer;
        }
        public string Send_option_Kasse(int numberTill, string state)
        {
            string answer = "unknown";
            string number = "0" + numberTill;

            answer = "/till/" + number + "/set/" + state;

            if (connect_state)
            {
                System.Console.WriteLine($" Answer send-<{answer}>");
                byte[] data = Encoding.UTF8.GetBytes(answer);
                int numberOfSentBytes = client.Send(data, data.Length);

                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                }
            }

            return answer;
        }
        private void BtnR1C7_Click(object sender, RoutedEventArgs e)
        {
            if (SCOEnable)
            {
                Send_SCO_CTRL(Convert.ToInt32(txtBTill.Text) + 8, "close");
            }
        }
        private void BtnR1C6_Click(object sender, RoutedEventArgs e)
        {
            if (SCOEnable)
            {
                Send_SCO_CTRL(Convert.ToInt32(txtBTill.Text) + 7, "close");
            }
        }
        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (SCOEnable)
            {
                Send_SCO_CTRL(Convert.ToInt32(txtBTill.Text) + 6, "close");
                //answer = "/till/" + number + "/set/open";
            }
            else
            {
                Send_OK(int.Parse(txtBThTill.Text));
            }
            /*
             * Sascha Dt O, [09.02.20 14:29]
                OK =

                Sascha Dt O, [09.02.20 14:29]
                If status = opening then we send :

                Sascha Dt O, [09.02.20 14:29]
                /till/03/set/open

                Sascha Dt O, [09.02.20 14:29]
                if status = open or closing then we send:

                Sascha Dt O, [09.02.20 14:30]
                /till/03/set/close
             */
            //Send_option_Kasse(int.Parse(txtBThTill.Text), "OK");
        }

        private void BtnBreak_Click(object sender, EventArgs e)
        {
            if (SCOEnable)
            {
                Send_SCO_CTRL(Convert.ToInt32(txtBTill.Text) + 5, "close");
            }
            else
            {
                if (KasseCurStateFunction.BREAK == true)
                {
                    Send_option_Kasse(int.Parse(txtBThTill.Text), "break/off");
                    KasseCurStateFunction.BREAK = false;

                }
                else
                {
                    Send_option_Kasse(int.Parse(txtBThTill.Text), "break/on");
                    KasseCurStateFunction.BREAK = true;

                }
            }
        }

        private void BtnML_Click(object sender, EventArgs e)/*CALLAC*/
        {
            if (SCOEnable)
            {
                Send_SCO_CTRL(Convert.ToInt32(txtBTill.Text) + 4, "close");
            }
            else
            {
                if (KasseCurStateFunction.MANAGER == true)
                {
                    Send_option_Kasse(int.Parse(txtBThTill.Text), "manager/off"); //Same with MA (Callac)
                    KasseCurStateFunction.MANAGER = false;
                }
                else
                {
                    Send_option_Kasse(int.Parse(txtBThTill.Text), "manager/on");
                    KasseCurStateFunction.MANAGER = true;
                }
            }
        }

        private void BtnMA_Click(object sender, EventArgs e)/*MANAGER*/
        {
            if (SCOEnable)
            {
                Send_SCO_CTRL(Convert.ToInt32(txtBTill.Text)+3, "close");
            }
            else
            {
                if (KasseCurStateFunction.CALLAC == true)
                {
                    Send_option_Kasse(int.Parse(txtBThTill.Text), "callac/off");
                    KasseCurStateFunction.CALLAC = false;
                }
                else
                {
                    Send_option_Kasse(int.Parse(txtBThTill.Text), "callac/on");
                    KasseCurStateFunction.CALLAC = true;
                }
              
            }
        }

        private void BtnSnooze_Click(object sender, EventArgs e)
        {
            if (SCOEnable)
            {
                Send_SCO_CTRL(Convert.ToInt32(txtBTill.Text) + 2, "close");
            }
            else
            {
                Send_option_Kasse(int.Parse(txtBThTill.Text), "snooze");
                KasseCurStateFunction.ALARM = false;
            }
        }

        private void BtnAlarm_Click(object sender, EventArgs e)
        {
            if (SCOEnable)
            {
                Send_SCO_CTRL(Convert.ToInt32(txtBTill.Text) + 1, "close");
            }
            else
            {
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
             connect_state = false;
             client.Close();
             Application.Current.Shutdown();
            
        }
        static bool CheckIp(string address) //check for correct IP addres
        {
            var nums = address.Split('.');
            int useless;
            return nums.Length == 4 && nums.All(n => int.TryParse(n, out useless)) &&
               nums.Select(int.Parse).All(n => n < 256);
        }
        private void TxtBIP_KeyPress(object sender, TextCompositionEventArgs e)
        {
            TextBox tbne = sender as TextBox;
            if ((!Char.IsDigit(e.Text, 0)) && (e.Text != "."))
            {
                { e.Handled = true; }
            }
            else
                if ((e.Text == ",") && ((tbne.Text.IndexOf(",") != -1) || (tbne.Text == "")))
            { e.Handled = true; }
           
        }
            private bool IsNumber(string Text)
            {
                int output;
                return int.TryParse(Text, out output);
            }    

        private void TxtBPort_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }

        private void TxtBTill_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Length < 1)
                txtBTill.Text = "1";
            if (e.Text.Length >8 )
                txtBTill.Text = "8";
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;

        }

        private void TxtBThTill_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Length < 1)
                txtBTill.Text = "1";
            if (e.Text.Length > 8)
                txtBTill.Text = "8";
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
            string s;
            INIManager inimanager = new INIManager(path);
            /* for reconnect to new NET param - need to reboot programm (need fix) */

            if (CheckIp(txtBIP.Text))
            {
                inimanager.WritePrivateString("net", "IP", txtBIP.Text);
                inimanager.WritePrivateString("net", "Port", txtBPort.Text);
                inimanager.WritePrivateString("net", "Tills", txtBTill.Text);
                inimanager.WritePrivateString("net", "NTill", txtBThTill.Text);

                if (cmbBoxWay.SelectedIndex == 1)
                    s = "Short";
                else if (cmbBoxWay.SelectedIndex == 2)
                    s = "Long";
                else
                    s = "Short";

                inimanager.WritePrivateString("net", "way", s);
                inimanager.WritePrivateString("net", "language", cmbBoxLang.Text);
                inimanager.WritePrivateString("net", "PortList", txtBPort_Listen.Text);

                inimanager.WritePrivateString("net", "SCOTill", txtSCoTill.Text);
                
                    
                inimanager.WritePrivateString("net", "Position1", txtNmBtnPos1.Text); inimanager.WritePrivateString("net", "Position2", txtNmBtnPos2.Text);
                inimanager.WritePrivateString("net", "Position3", txtNmBtnPos3.Text); inimanager.WritePrivateString("net", "Position4", txtNmBtnPos4.Text);
                inimanager.WritePrivateString("net", "Position5", txtNmBtnPos5.Text); inimanager.WritePrivateString("net", "Position6", txtNmBtnPos6.Text);
                               
                inimanager.WritePrivateString("net", "chkAlarm", Convert.ToString(chkAlarm.IsChecked));
                inimanager.WritePrivateString("net", "chkMA", Convert.ToString(chkMA.IsChecked));
                inimanager.WritePrivateString("net", "chkML", Convert.ToString(chkML.IsChecked));
                inimanager.WritePrivateString("net", "chkOK", Convert.ToString(chkOK.IsChecked));
                inimanager.WritePrivateString("net", "chkPause", Convert.ToString(chkPause.IsChecked));
                inimanager.WritePrivateString("net", "chkSnooze", Convert.ToString(chkSnooze.IsChecked));
                inimanager.WritePrivateString("net", "buttonFontSize", Convert.ToString(txtFontSizeButton.Text));
                inimanager.WritePrivateString("net", "buttonRadius", Convert.ToString(txtRadiusButton.Text));



                createConnect();
            }
            else                
            MessageBox.Show("Wrong IP", "NET Param", MessageBoxButton.OK, MessageBoxImage.Warning);


        }
        private void TxtBPort_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void Check_button_to_visible()
        {
            if (SCOEnable)
            {
                btnSCO.Content = "KLS";
                btnAlarm.Style = (Style)Application.Current.Resources["CircleButton"];
                Grid.SetColumn(btnAlarm, 0);
                Grid.SetColumn(btnSnooze, 1);
                Grid.SetColumn(btnML, 3);
                Grid.SetColumn(btnMA, 2);
                Grid.SetColumn(btnBreak, 4);
                Grid.SetColumn(btnOk, 5);

                for (int i = 0; i < 8; i++)
                {
                    btns[i].Visibility = Visibility.Hidden;                    
                }
                    for (int i = 0; i < int.Parse(txtSCoTill.Text); i++)
                {
                    btns[i].Visibility = Visibility.Visible;
                    btns[i].Background = Brushes.Green;
                    btns[i].Foreground = Brushes.Black;
                    btns[i].Content = Convert.ToString(int.Parse(txtBTill.Text) + i + 1);
                    btns[i].FontSize = int.Parse(txtFontSizeButton.Text);
                }
                for (int i = 8; i < 16; i++)
                {
                    btns[i].Visibility = Visibility.Hidden;                    
                }
                for (int i = 8; i < int.Parse(txtSCoTill.Text) + 8; i++)
                {
                    btns[i].Background = Brushes.Red;
                    btns[i].Foreground = Brushes.Black;
                    btns[i].Visibility = Visibility.Visible;
                    btns[i].Content = Convert.ToString(int.Parse(txtBTill.Text) + (i-8)  + 1);
                    btns[i].FontSize = int.Parse(txtFontSizeButton.Text);
                }


            }
            else
            {
                btnSCO.Content = "SCO";
                btnAlarm.Style = (Style)Application.Current.Resources["CircleNonButton"];
                Grid.SetColumn(btnAlarm, Convert.ToInt32(txtNmBtnPos1.Text)-1);
                Grid.SetColumn(btnSnooze, Convert.ToInt32(txtNmBtnPos2.Text) - 1);
                Grid.SetColumn(btnML, Convert.ToInt32(txtNmBtnPos4.Text) - 1);
                Grid.SetColumn(btnMA, Convert.ToInt32(txtNmBtnPos3.Text) - 1);
                Grid.SetColumn(btnBreak, Convert.ToInt32(txtNmBtnPos5.Text) - 1);
                Grid.SetColumn(btnOk, Convert.ToInt32(txtNmBtnPos6.Text) - 1);

                for (int i = 0; i < 8; i++)
                {
                    btns[i].Visibility = Visibility.Hidden;
                    btns[i].FontSize = int.Parse(txtFontSizeButton.Text);

                }
                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {
                    btns[i].Visibility = Visibility.Visible;
                    
                }
                for (int i = 8; i < 16; i++)
                {
                    btns[i].FontSize = int.Parse(txtFontSizeButton.Text);
                }
                string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";               
                INIManager inimanager = new INIManager(path);

                if (inimanager.GetPrivateString("net", "nameKASSE1") == "")
                {
                    BtnKasse1.Content = "KASSE1";
                }
                else
                    BtnKasse1.Content = inimanager.GetPrivateString("net", "nameKASSE1");

                if (inimanager.GetPrivateString("net", "nameKASSE2") == "")
                {
                    BtnKasse2.Content = "KASSE2";
                }
                else
                    BtnKasse2.Content = inimanager.GetPrivateString("net", "nameKASSE2");

                if (inimanager.GetPrivateString("net", "nameKASSE3") == "")
                {
                    BtnKasse3.Content = "KASSE3";
                }
                else
                    BtnKasse3.Content = inimanager.GetPrivateString("net", "nameKASSE3");

                if (inimanager.GetPrivateString("net", "nameKASSE4") == "")
                {
                    BtnKasse4.Content = "KASSE4";
                }
                else
                    BtnKasse4.Content = inimanager.GetPrivateString("net", "nameKASSE4");

                if (inimanager.GetPrivateString("net", "nameKASSE5") == "")
                {
                    BtnKasse5.Content = "KASSE5";
                }
                else
                    BtnKasse5.Content = inimanager.GetPrivateString("net", "nameKASSE5");

                if (inimanager.GetPrivateString("net", "nameKASSE6") == "")
                {
                    BtnKasse6.Content = "KASSE6";
                }
                else
                    BtnKasse6.Content = inimanager.GetPrivateString("net", "nameKASSE6");

                if (inimanager.GetPrivateString("net", "nameKASSE7") == "")
                {
                    BtnKasse7.Content = "KASSE7";
                }
                else
                    BtnKasse7.Content = inimanager.GetPrivateString("net", "nameKASSE7");

                if (inimanager.GetPrivateString("net", "nameKASSE8") == "")
                {
                    BtnKasse8.Content = "KASSE8";
                }
                else
                    BtnKasse8.Content = inimanager.GetPrivateString("net", "nameKASSE8");



                /*                btnML.Content = "ML";
                                btnMA.Content = "MA";
                                btnOk.Content = "OK";
                                btnAlarm.Content = "ALARM";
                                btnSnooze.Content = "SNOOZE";
                                btnBreak.Content = "PAUSE";*/
                btnML.Content = btnXMLname.btn_ML;
                btnMA.Content = btnXMLname.btn_MA;
                btnOk.Content = btnXMLname.btn_OK;
                btnAlarm.Content = btnXMLname.btn_Alarm;
                btnSnooze.Content = btnXMLname.btn_Snooze;
                btnBreak.Content = btnXMLname.btn_PAUSE; 
                
                BtnR1C6.Content = " ";
                BtnR1C7.Content = " ";

                BtnR1C6.Visibility = Visibility.Hidden;
                BtnR1C7.Visibility = Visibility.Hidden;

                BtnR1C6.Background = Brushes.Gray;
                BtnR1C6.Foreground = Brushes.White;
                BtnR1C7.Background = Brushes.Gray;
                BtnR1C7.Foreground = Brushes.White;

                btnSnooze.Background = Brushes.Gray;
                btnSnooze.Foreground = Brushes.White;

                btnOk.Background = Brushes.Gray;
                btnOk.Foreground = Brushes.White;

                if (chkML.IsChecked == true)
                    btnML.Visibility = Visibility.Visible;
                else
                    btnML.Visibility = Visibility.Hidden;

                if (chkOK.IsChecked == true)
                    btnOk.Visibility = Visibility.Visible;
                else
                    btnOk.Visibility = Visibility.Hidden;

                if (chkMA.IsChecked == true)
                    btnMA.Visibility = Visibility.Visible;
                else
                    btnMA.Visibility = Visibility.Hidden;

                if (chkPause.IsChecked == true)
                    btnBreak.Visibility = Visibility.Visible;
                else
                    btnBreak.Visibility = Visibility.Hidden;

                if (chkAlarm.IsChecked == true)
                    btnAlarm.Visibility = Visibility.Visible;
                else
                    btnAlarm.Visibility = Visibility.Hidden;

                if (chkSnooze.IsChecked == true)
                    btnSnooze.Visibility = Visibility.Visible;
                else
                    btnSnooze.Visibility = Visibility.Hidden;


            }
        }
        private void ManagerviewToolStripMenuItem_hm(object sender, MouseButtonEventArgs e)
        {
            for (int i = 0; i < 8; i++)
                btns[i].Visibility = Visibility.Hidden;

            for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;

            groupBoxConfig.Visibility = Visibility.Visible;
            DockPanelManager.Visibility = Visibility.Hidden;

           // Check_button_to_visible();
        }

        private void ConfigurationToolStripMenuItem_hm(object sender, MouseButtonEventArgs e)
        {
            for (int i = 0; i < Convert.ToInt32(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;
            numberway = cmbBoxWay.SelectedIndex;
            if (numberway == 1)/*short way no. Not hidden. Only the letters ok should be away*/
                btnOk.Content = "";
            else
                btnOk.Content = "OK";

            
            currentThill = int.Parse(txtBThTill.Text);

            groupBoxConfig.Visibility = Visibility.Hidden;
            DockPanelManager.Visibility = Visibility.Visible;
        }

        private void ManagerviewToolStripMenuItem_hm(object sender, KeyEventArgs e)
        {

        }       

        private void txtNameButton_KeyDown1(object sender, KeyEventArgs e)
        {
            if (Convert.ToInt32(e.Key) == 0)
            {
                txtBTill.Text = "Alarm";
            }
            if (e.Key == Key.Enter)
            {   
                txtNameButton1.Visibility = Visibility.Hidden;                
                BtnKasse1.Content = txtNameButton1.Text;
                BtnKasse1.Visibility = Visibility.Visible;

                string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
                INIManager inimanager = new INIManager(path);
                /* for reconnect to new NET param - need to reboot programm (need fix) */                
                inimanager.WritePrivateString("net", "nameKASSE1", txtNameButton1.Text);                
            }            
        }
        private void txtNameButton_KeyDown2(object sender, KeyEventArgs e)
        {            
            if (e.Key == Key.Enter)
            {
                txtNameButton2.Visibility = Visibility.Hidden;
                BtnKasse2.Content = txtNameButton2.Text;
                BtnKasse2.Visibility = Visibility.Visible;

                string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
                INIManager inimanager = new INIManager(path);
                /* for reconnect to new NET param - need to reboot programm (need fix) */
                inimanager.WritePrivateString("net", "nameKASSE2", txtNameButton2.Text);
            }
        }
        private void txtNameButton_KeyDown3(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtNameButton3.Visibility = Visibility.Hidden;
                BtnKasse3.Content = txtNameButton3.Text;
                BtnKasse3.Visibility = Visibility.Visible;

                string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
                INIManager inimanager = new INIManager(path);
                /* for reconnect to new NET param - need to reboot programm (need fix) */
                inimanager.WritePrivateString("net", "nameKASSE3", txtNameButton3.Text);
            }
        }
        private void txtNameButton_KeyDown4(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtNameButton4.Visibility = Visibility.Hidden;
                BtnKasse4.Content = txtNameButton4.Text;
                BtnKasse4.Visibility = Visibility.Visible;

                string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
                INIManager inimanager = new INIManager(path);
                /* for reconnect to new NET param - need to reboot programm (need fix) */
                inimanager.WritePrivateString("net", "nameKASSE4", txtNameButton4.Text);
            }
        }
        private void txtNameButton_KeyDown5(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtNameButton5.Visibility = Visibility.Hidden;
                BtnKasse5.Content = txtNameButton5.Text;
                BtnKasse5.Visibility = Visibility.Visible;

                string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
                INIManager inimanager = new INIManager(path);
                /* for reconnect to new NET param - need to reboot programm (need fix) */
                inimanager.WritePrivateString("net", "nameKASSE5", txtNameButton5.Text);
            }
        }
        private void txtNameButton_KeyDown6(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtNameButton6.Visibility = Visibility.Hidden;
                BtnKasse6.Content = txtNameButton6.Text;
                BtnKasse6.Visibility = Visibility.Visible;

                string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
                INIManager inimanager = new INIManager(path);
                /* for reconnect to new NET param - need to reboot programm (need fix) */
                inimanager.WritePrivateString("net", "nameKASSE6", txtNameButton6.Text);
            }
        }
        private void txtNameButton_KeyDown7(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtNameButton7.Visibility = Visibility.Hidden;
                BtnKasse7.Content = txtNameButton7.Text;
                BtnKasse7.Visibility = Visibility.Visible;

                string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
                INIManager inimanager = new INIManager(path);
                /* for reconnect to new NET param - need to reboot programm (need fix) */
                inimanager.WritePrivateString("net", "nameKASSE7", txtNameButton7.Text);
            }
        }
        private void txtNameButton_KeyDown8(object sender, KeyEventArgs e)
        {
           
            if (e.Key == Key.Enter)
            {
                txtNameButton8.Visibility = Visibility.Hidden;
                BtnKasse8.Content = txtNameButton8.Text;
                BtnKasse8.Visibility = Visibility.Visible;

                string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
                INIManager inimanager = new INIManager(path);
                /* for reconnect to new NET param - need to reboot programm (need fix) */
                inimanager.WritePrivateString("net", "nameKASSE8", txtNameButton8.Text);
            }
           

        }
        private void numberSetupButtonPosition(object sender, TextCompositionEventArgs e)
        {
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
            if ((Convert.ToInt32(e.Text) < 1) || ((Convert.ToInt32(e.Text) > 9)))
            {
                MessageBox.Show("Wrong number position", "Position number must be in the range from 1 to 8", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
        private void BtnKasse1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!SCOEnable)
            {
                BtnKasse1.Visibility = Visibility.Hidden;
                txtNameButton1.Visibility = Visibility.Visible;
            }
        }
        private void BtnKasse2_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!SCOEnable)
            {
                BtnKasse2.Visibility = Visibility.Hidden;
                txtNameButton2.Visibility = Visibility.Visible;
            }
        }

        private void BtnKasse3_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!SCOEnable)
            {
                BtnKasse3.Visibility = Visibility.Hidden;
                txtNameButton3.Visibility = Visibility.Visible;
            }
        }

        private void BtnKasse4_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!SCOEnable)
            {
                BtnKasse4.Visibility = Visibility.Hidden;
                txtNameButton4.Visibility = Visibility.Visible;
            }
        }

        private void BtnKasse5_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!SCOEnable)
            {
                BtnKasse5.Visibility = Visibility.Hidden;
                txtNameButton5.Visibility = Visibility.Visible;
            }
        }

        private void BtnKasse6_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!SCOEnable)
            {
                BtnKasse6.Visibility = Visibility.Hidden;
                txtNameButton6.Visibility = Visibility.Visible;
            }
        }

        private void BtnKasse7_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!SCOEnable)
            {
                BtnKasse7.Visibility = Visibility.Hidden;
                txtNameButton7.Visibility = Visibility.Visible;
            }
        }

        private void BtnKasse8_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!SCOEnable)
            {
                BtnKasse8.Visibility = Visibility.Hidden;
                txtNameButton8.Visibility = Visibility.Visible;
            }
        }

        private void cmbBoxLang_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           // apply_language();
        }

        private void WindowCUCOS_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            Settings.Default.Save();
            base.OnClosing(e);
        }

        private void txtFontSizeButton_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Length < 1)
                txtFontSizeButton.Text = "1";
            if (e.Text.Length > 32)
                txtFontSizeButton.Text = "32";
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }

        private void txtRadiusButton_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
           /* if ((Convert.ToInt32(e.Text) < 43) || ((Convert.ToInt32(e.Text) > 100)))
            {
                MessageBox.Show("Wrong radius", "MIN radius = 43, MAX = 100", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
                if (e.Text.Length < 43)
                txtRadiusButton.Text = "43";
            if (e.Text.Length > 200)
                txtRadiusButton.Text = "200";*/
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SCOEnable = !SCOEnable;
            Check_button_to_visible();   
        }
    }
}
